from urllib import urlopen;
from bs4 import BeautifulSoup;
from urlparse import urljoin
import json
import datetime

now = datetime.datetime.now() + datetime.timedelta(days=1)


def replacer(txt):
    base = "http://www.crossfitexcel.com"
    wod = "/wod/"
    viewMore = "../../images/home-wod-viewmore.png"
    imgSrc = "../../photos"

    txt = txt.replace(wod, base + wod)
    txt = txt.replace(viewMore, base + "/images/home-wod-viewmore.png")
    txt = txt.replace(imgSrc, base + "/photos")
    txt = txt.replace("\n", "").replace("\r", "").replace("b'", "'").replace("\"", "'")
    return str(
        txt.replace("\\n", "")
        .replace("\\r", "")
        .replace("b'", "'")
        .replace("\"", "'")).decode('utf8', 'ignore')


def getTmrw(finalId):
    id = int(finalId) + 1
    base = "http://www.crossfitexcel.com"
    url = base + "/wod/" + str(id)
    noWodTxt = "We are sorry, but this Wod does not exist!"
    trmwHTML = urlopen(url).read()
    rcrd = {}
    tmrwSoup = BeautifulSoup(trmwHTML, 'html.parser')
    tmrwPosts = tmrwSoup.findAll("div", {"id": "wodboxind"})
    if not noWodTxt in str(tmrwPosts):
        for post in tmrwPosts:
            txt = ("".join([str(x) for x in post.contents])).decode('unicode_escape').encode(
                'ascii', 'ignore')
            for link in post.findAll('img'):
                if "viewvidfb.php" not in link:
                    img = urljoin(base + "/", str(link['src']))
                    rcrd["wodid"] = id
                    rcrd["url"] = url
                    rcrd["img"] = img.replace("photos/t-", "photos/")
                    rcrd["txt"] = replacer(txt).replace("\n", "").replace("\r", "").replace("b'",
                                                                                            "'").replace(
                        "\"", "'")
                    rcrd["title"] = now.strftime("%b") + " " + now.strftime("%d")
                    rcrd["id"] = 0
                    rcrd["refreshDate"] = "Last Refreshed @ " + str(datetime.datetime.now())
                    return rcrd


# https://www.pythonanywhere.com/#id_hosting_details

base = "http://www.crossfitexcel.com"
page = base + "/wods/wod/"
count = 0
html = urlopen(page).read()
soup = BeautifulSoup(html, 'html.parser')

posts = soup.findAll("div", {"id": "wodbox"})  # use find_all for all of them

feeds = [];

# print [post.contents for post in posts]
for post in posts:
    if count > 7:
        break;
    txt = ("".join([str(x.prettify('latin-1')) for x in post.contents]))
    try:
        title = post.find("span", {"class": "woddatemonth"}).string + " " + post.find("span", {
            "class": "woddatenumber"}).string
    except:
        title = ""
    rcrd = {}
    for link in post.find_all('a'):
        if link.find('img'):
            imgSource = str(link.find('img')['src'])
            if "../../images/home-wod-viewmore.png" not in imgSource:
                url = urljoin(page, link.get('href'))
                img = urljoin(base + "/", (imgSource).replace("../../", ""))

                id = (url.replace("http://www.crossfitexcel.com/wod/", ""))
                if count == 0:
                    firstRecord = (getTmrw(id))
                    if firstRecord:
                        feeds.append(firstRecord)
                        count += 1
                rcrd["wodid"] = id
                rcrd["url"] = url
                rcrd["img"] = img
                rcrd["txt"] = str(replacer(txt))
                rcrd["title"] = title
                rcrd["id"] = count
                rcrd["refreshDate"] = "Last Refreshed @ " + str(datetime.datetime.now())
                feeds.append(rcrd)
                count = count + 1

# return json.dumps(feeds,sort_keys=True, indent=4)

with open("/home/rbeall49/feeds/feeds/myapp/feed.txt", "wb") as fo:
    fo.write(json.dumps(feeds, sort_keys=True, indent=4));