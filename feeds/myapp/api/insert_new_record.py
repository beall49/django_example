import sqlite3 as sqlite
def insert_post(post):
    INSERT = "INSERT INTO tbl_articles (current_id, title, image, url, post_text, clean_date) VALUES ({0},'{1}','{2}','{3}','{4}', {5})"
    DB_NAME = '/home/rbeall49/feeds/feeds/myapp/wod.db'
    with sqlite.connect(DB_NAME) as con:
        cur = con.cursor()
        if cur.execute("select * from tbl_articles where current_id={0}".format(post.current_id)).fetchone() is None:
            INSERT = INSERT.format(post.current_id, post.title, post.image, post.url, post.post_text, post.clean_date)
            cur.execute(INSERT)
            return True
    return False