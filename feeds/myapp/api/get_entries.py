import sqlite3 as sqlite
import json

DB_NAME = '/home/rbeall49/feeds/feeds/myapp/wod.db'
"""
    pass in a  number of records you'd like to see
"""
def sql_select(records):
    with sqlite.connect(DB_NAME) as con:
        cur = con.cursor()
        cur.execute("select * from tbl_articles order by current_id desc limit {0};".format( records ))
        return cur.fetchall(), list(map(lambda x: str(x[0]), cur.description))


def return_entries(records=10):
    record_set, columns = sql_select(records)
    return json.dumps([dict(zip([str(cols) for cols in columns], [str(col) for col in row])) for row in record_set], indent=1, separators=(',', ':'))


def return_entry(current_id=""):
    if current_id == "":
        return return_entries(1)
    with sqlite.connect(DB_NAME) as con:
        cur = con.cursor()
        cur.execute("select * from tbl_articles where current_id = {0}".format( current_id ))
        rows = cur.fetchall()
        columns = list(map(lambda x: x[0], cur.description))
        return json.dumps([dict(zip(columns, row)) for row in rows], indent=1)

def return_entry_by_date(incoming_date):
    with sqlite.connect(DB_NAME) as con:
        cur = con.cursor()
        SELECT = "select * from tbl_articles where clean_date = {0}".format( incoming_date )
        cur.execute(SELECT)
        rows = cur.fetchall()
        if rows:
            columns = list(map(lambda x: x[0], cur.description))
            return json.dumps([dict(zip(columns, row)) for row in rows], indent=1)

        return '{"No Entry on That Date":"Skwat"}'
