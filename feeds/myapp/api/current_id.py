import sqlite3 as sqlite

def get_current_id():
    DB_NAME = '/home/rbeall49/feeds/feeds/myapp/wod.db'
    MAX_ID = 'select max(current_id) FROM tbl_articles'
    with sqlite.connect(DB_NAME) as con:
        cur = con.cursor()
        record_set = cur.execute(MAX_ID)
        for row in record_set:
           return int(row[0]) + 1 if row[0] else 0