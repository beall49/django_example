import sys
if sys.version_info[0] == 2:
    import check_for_new_post as post
else:

    import feeds.myapp.api.check_for_new_post as post


if post.check_for_new_post():
    print ('{"New Record Inserted":}')
else:
    print ('{"No New Record Inserted":}')