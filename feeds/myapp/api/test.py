import sys
from bs4 import BeautifulSoup as BS
from collections import namedtuple
import unidecode
from urllib import urlopen
# if sys.version_info[0] == 2:

# else:
#     from urllib.request import urlopen


def get_response(URL):
    return BS(urlopen(URL).read(), 'html.parser')

def replace_extras(incoming_string):
    replace_dict = {"\"":"","Time": "","PreWOD": "", "Load": "","Total Score": "","Load": "","Total Score": "","Met Con": "Met Con \n","Strength": "Strength \n","AMRAP": "\n", "'":""}
    return_string = incoming_string.lstrip(' ')


    for key, val in replace_dict.iteritems():
        return_string = return_string.replace(key, val)
    return return_string

def check_for_new_post(): #check_for_new_post()
    Post = namedtuple('Post', 'current_id, title, image, url, post_text, clean_date')
    NO_WOD_TEXT = "We are sorry, but this Wod does not exist!"
    base ="http://www.crossfitexcel.com"
    _id = str(804)
    URL = base + "/wod/" + _id

    soup= get_response(URL)
    news= soup.findAll("div", {"id": "wodboxind"})
    img, data, date = "", "", ""
    for article in news:
        date = article.find("div", {"id": "wodbottomind"}).text.replace('Posted on ','')
        image = (article.find("img")['src']).replace('t-','')

        #data found in table
        post_data = article.find("div", {"id": "wodinnerind"}).find("table")
        print ''.join(td for table in post_data.findAll("table") for td in table.findAll("td"))
        post_text = unidecode.unidecode(' \n'.join(td.text for table in post_data.findAll("table") for td in table.findAll("td") ))


