from django.http import HttpResponse
from feeds.myapp.api import current_id as get_id
from feeds.myapp.api import get_entries
from feeds.myapp.api import check_for_new_post
from feeds.myapp.api import send_to_parse as parse

'''api'''

""" trys to insert new entry"""
def check_for_update(request):
    if check_for_new_post.check_for_new_post():
        parse.save_new_record()
        return HttpResponse(get_entries.return_entries(1), content_type='application/json')

    return HttpResponse('{"No New Record Inserted":""}', content_type='application/json')



""" trys to return entry based on passed in parm"""
def return_single_day(request, current_id=""):
    #if current_id is blank or not a number return the last entry
    if current_id == "" or not str(current_id).isdigit():
        return HttpResponse(get_entries.return_entries(1), content_type='application/json')

    entry = get_entries.return_entry(current_id)
    return HttpResponse(entry, content_type='application/json')


"""gets max current_id"""
def get_current_id(request):
    return HttpResponse('{"current_id": ' + str(get_id.get_current_id()) + '}', content_type='application/json')



def get_records(request, records=10):
    if len(records) == 0:
        records = 10
    if not (str(records).isdigit()):
        records = 10
    entries = get_entries.return_entries(int(records))

    return HttpResponse(entries, content_type=u'application/json')


''' get entry by date'''
def get_record_by_date(request, incoming_date):
    entry = get_entries.return_entry_by_date(incoming_date)
    return HttpResponse(entry, content_type='application/json')



def ck_data(request):
    jsonData = """[{
    "color": "black",
    "current_id": 1,
    "description": "<ul><li><li>100% nylon 330D with water proof coating</li><li>Unlined hood </li><li>Self neck tape</li><li>Antique brass eyelets</li><li>Antique brass 6 snap front closure </li><li>Elastic cuffs</li><li>Underarm grommets</li><li>Drawcord closure at hood and bottom opening</li><li>Standard fit </li></ul>",
    "image": "http://static1.squarespace.com/static/51cf4eeae4b065321a7938c3/51cf4eeae4b065321a7938d5/565605cee4b0509ba9daec8d/1448478228158/DSC_2343.JPG?format=500w",
    "itemId": "5654dd07e4b0106dfb635ec5",
    "price": "85.00",
    "title": "The Coaches Jacket",
    "url": "http://caffeineandkilos.com/shop/the-coaches-jacket"
},
{
    "color": "black",
    "current_id": 2,
    "description": "<ul><li><li>100% nylon 330D with water proof coating</li><li>Unlined hood </li><li>Self neck tape</li><li>Antique brass eyelets</li><li>Antique brass 6 snap front closure </li><li>Elastic cuffs</li><li>Underarm grommets</li><li>Drawcord closure at hood and bottom opening</li><li>Standard fit </li></ul>",
    "image": "http://static1.squarespace.com/static/51cf4eeae4b065321a7938c3/51cf4eeae4b065321a7938d5/565605cee4b0509ba9daec8d/1448478228158/DSC_2343.JPG?format=500w",
    "itemId": "5654dd07e4b0106dfb635ec5",
    "price": "85.00",
    "title": "The Coaches Jacket",
    "url": "http://caffeineandkilos.com/shop/the-coaches-jacket"
},
{
    "color": "black",
    "current_id": 3,
    "description": "<ul><li><li>100% nylon 330D with water proof coating</li><li>Unlined hood </li><li>Self neck tape</li><li>Antique brass eyelets</li><li>Antique brass 6 snap front closure </li><li>Elastic cuffs</li><li>Underarm grommets</li><li>Drawcord closure at hood and bottom opening</li><li>Standard fit </li></ul>",
    "image": "http://static1.squarespace.com/static/51cf4eeae4b065321a7938c3/51cf4eeae4b065321a7938d5/565605cee4b0509ba9daec8d/1448478228158/DSC_2343.JPG?format=500w",
    "itemId": "5654dd07e4b0106dfb635ec5",
    "price": "85.00",
    "title": "The Coaches Jacket",
    "url": "http://caffeineandkilos.com/shop/the-coaches-jacket"
},
{
    "color": "black",
    "current_id": 4,
    "description": "<ul><li><li>100% nylon 330D with water proof coating</li><li>Unlined hood </li><li>Self neck tape</li><li>Antique brass eyelets</li><li>Antique brass 6 snap front closure </li><li>Elastic cuffs</li><li>Underarm grommets</li><li>Drawcord closure at hood and bottom opening</li><li>Standard fit </li></ul>",
    "image": "http://static1.squarespace.com/static/51cf4eeae4b065321a7938c3/51cf4eeae4b065321a7938d5/565605cee4b0509ba9daec8d/1448478228158/DSC_2343.JPG?format=500w",
    "itemId": "5654dd07e4b0106dfb635ec5",
    "price": "85.00",
    "title": "The Coaches Jacket",
    "url": "http://caffeineandkilos.com/shop/the-coaches-jacket"
},
{
    "color": "black",
    "current_id": 5,
    "description": "<ul><li><li>100% nylon 330D with water proof coating</li><li>Unlined hood </li><li>Self neck tape</li><li>Antique brass eyelets</li><li>Antique brass 6 snap front closure </li><li>Elastic cuffs</li><li>Underarm grommets</li><li>Drawcord closure at hood and bottom opening</li><li>Standard fit </li></ul>",
    "image": "http://static1.squarespace.com/static/51cf4eeae4b065321a7938c3/51cf4eeae4b065321a7938d5/565605cee4b0509ba9daec8d/1448478228158/DSC_2343.JPG?format=500w",
    "itemId": "5654dd07e4b0106dfb635ec5",
    "price": "85.00",
    "title": "The Coaches Jacket",
    "url": "http://caffeineandkilos.com/shop/the-coaches-jacket"
}]"""
    return HttpResponse(jsonData, content_type='application/json')

'''api'''

