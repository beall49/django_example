from parse_rest.connection import register
from parse_rest.datatypes import Object

import sys
if sys.version_info[0] == 2:
    import get_entries as entry
else:
    from feeds.myapp.api import get_entries as entry

def save_new_record():
    application_id = "8rKmv9RfYnu9dZ5zKyEMa4wlHbmGNpapHzLxBEkO"
    rest_api_key = "3hidri1tNdkMILtv2CM66ppLR7rw8tlQP10k8tx8"

    register(application_id, rest_api_key)

    class Entry(Object):
        pass
    record_set, columns = entry.sql_select(1)

    entry_object = Entry()
    for row in record_set:
        entry_object.id = row[0]
        entry_object.current_id = row[1]
        entry_object.title = row[2]
        entry_object.image = row[3]
        entry_object.url = row[4]
        entry_object.post_text = row[5]
        entry_object.date_add = row[6]
        entry_object.clean_date = row[7]
    entry_object.save()


