import sys
from bs4 import BeautifulSoup as BS
from collections import namedtuple
import unidecode

if sys.version_info[0] == 2:
    from urllib import urlopen
    import current_id
    import insert_new_record as insert
else:
    from urllib.request import urlopen
    import feeds.myapp.api.current_id as current_id
    import feeds.myapp.api.insert_new_record as insert

def clean_date(long_date):
    import re
    from datetime import datetime
    reg = "(?<=[0-9])(?:st|nd|rd|th)"
    original_date =  re.sub(reg, "", long_date)
    stripped_date = datetime.strptime(original_date, '%A %B %d, %Y')
    return '{d.year}{d.month:02}{d.day:02}'.format(d=stripped_date)

def get_response(URL):
    return BS(urlopen(URL).read(), 'html.parser')

def replace_extras(incoming_string):
    replace_dict = {"\"":"","Time": "","PreWOD": "", "Load": "","Total Score": "","Load": "","Total Score": "","Met Con": "Met Con \n","Strength": "Strength \n","AMRAP": "\n", "'":""}
    return_string = incoming_string.lstrip(' ')

    if sys.version_info[0] == 2:
        for key, val in replace_dict.iteritems():
            return_string = return_string.replace(key, val)
    else:
        for key, val in replace_dict.items():
            return_string = return_string.replace(key, val)

    return return_string

def check_for_new_post(): #check_for_new_post()
    Post = namedtuple('Post', 'current_id, title, image, url, post_text, clean_date')
    NO_WOD_TEXT = "We are sorry, but this Wod does not exist!"
    base ="http://www.crossfitexcel.com"
    _id = str(current_id.get_current_id())
    URL = base + "/wod/" + _id

    soup= get_response(URL)
    news= soup.findAll("div", {"id": "wodboxind"})

    if NO_WOD_TEXT in str(news):
        return False
    else:
        img, data, date = "", "", ""
        for article in news:
            date = article.find("div", {"id": "wodbottomind"}).text.replace('Posted on ','')
            image = (article.find("img")['src']).replace('t-','')

            #data found in table
            post_data = article.find("div", {"id": "wodinnerind"}).find("table")

            post_text = ""
            for table in post_data.findAll("table"):
                for td in table.findAll("td"):
                    if len(td.findAll("span")) > 0:
                        for span in td.findAll("span"):
                            post_text = post_text + ' \n' + span.text
                    else:
                        post_text = post_text + ' \n' + td.text

            post_text = unidecode.unidecode(post_text)

            post = Post(_id, date, base + image, URL, replace_extras(post_text), clean_date(date))
            return insert.insert_post(post)
    return False


