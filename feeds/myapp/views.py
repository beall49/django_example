from datetime import datetime
from django.shortcuts import render
from django.http import HttpResponse
from feeds.myapp.getFeeds import getTheFeeds as myFeed
import json





"""OLD FEED STUFF, NEED TO DEPRECATE"""
def returnfeed(request):
    f=myFeed()
    return render(request, 'feed.html', {'feedHere': (f)})


"""FRONT PAGE STUFF FOR THE KIDS"""
def input(request):
    return render(request, 'input.html')

def wordList(request, strParm=""):
    if strParm == "":
        return HttpResponse(json.dumps('{"Sorry": "Sumping is wong"}'), content_type='application/json')
    else:
        return abcOrder(request, strParm)

def syllables(request, strParm=""):
    if strParm == "":
        return HttpResponse(json.dumps('{"Sorry": "Sumping is wong"}'), content_type='application/json')
    else:
        return getSyllables(request, strParm)

def getSyllables(request, strParm):
    from hyphen import Hyphenator

    #your language english
    h_en = Hyphenator('en_US')

    #this makes sure the words come out in english
    style = 'utf-8'
    wordList.extend(word.strip() for word in wordList.replace("\n", "").split(","))

    words =[]
    #for each words in your word list
    for word in wordList:
        #this cuts the word into syllables
        brokenUpWord      = '-'.join(h_en.syllables(word.decode(style)))

        #this gets the count of syllables
        countOfSyllables        = str(len(str(brokenUpWord).split('-')))

        #print them out
        words.extend(brokenUpWord +';', countOfSyllables + ' syllable' + ('s' if countOfSyllables>1 else '') +'\n')

    return HttpResponse( json.dumps({'words': words}), content_type='application/json')


def abcOrder(request, strParm):
    #put a list of words in here, seperated by a comma
    #"""speech, claim, mild, waist, sway, strike, stray, beast, fade, stain, fleet, sign, leaf,stride, thigh, praise, thief, slight, height, campaign, describe, cease, sacrifice, plight"""
    wordList = strParm

    #this is removing any spaces, and putthing the words
    #in a list seperated by commas
    words=[]
    words.extend(word.strip() for word in wordList.replace("\n", "").split(","))

    #for each word in your list of words
    #sort the list, then print the word
    myWordList = json.dumps({'wordList': sorted(words,  key=str.lower)})
    return HttpResponse(myWordList, content_type='application/json')



"""old stuff, don't think using anymore """
def home(request):
     return render(request, 'home.html', {'right_now':datetime.utcnow()})