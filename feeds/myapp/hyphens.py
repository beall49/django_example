from hyphen import Hyphenator, dict_info
from hyphen.dictools import *

# Download and install some dictionaries in the default directory using the default
# repository, usually the LibreOffice website
install('en_US')