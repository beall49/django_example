import sqlite3 as sqlite
import json

DB_NAME = '/home/rbeall49/feeds/feeds/myapp/wod.db'
def get_current_id():
    MAX_ID = 'select max(current_id) FROM tbl_articles'
    with sqlite.connect(DB_NAME) as con:
        cur = con.cursor()
        record_set = cur.execute(MAX_ID)
        for row in record_set:
            return int(row[0])
"""
    get columns and record set
"""
def sql_select(current_id):
    with sqlite.connect(DB_NAME) as con:
        cur = con.cursor()
        cur.execute("select * from tbl_articles where current_id = {0}".format( current_id ))
        return cur.fetchall(), list(map(lambda x: x[0], cur.description))


def return_last_entry(current_id=""):
    if len(current_id) == 0:
        current_id = get_current_id()
    record_set, columns = sql_select(current_id)

    return json.dumps([dict(zip(columns, row)) for row in record_set], indent=1)