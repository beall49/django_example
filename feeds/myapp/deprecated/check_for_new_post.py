
import re
import unidecode
import sys
from bs4 import BeautifulSoup
from collections import namedtuple

if sys.version_info[0] == 2:
    from urllib import urlopen
else:
    from urllib.request import urlopen
def check_for_new_post(current_id):
    IMG_REGEX = r"<img[^>]+src\s*=\s*['\"]([^'\"]+)['\"][^>]*>"
    Post = namedtuple('Post', 'current_id, title, image, url, post_text')
    base = "http://www.crossfitexcel.com"
    page = base + "/wod/{0}"
    url = page.format(current_id)
    post_text = "We are sorry, but this Wod does not exist!"

    html = urlopen(url).read()
    content = BeautifulSoup(html, 'html.parser')
    soup = content.findAll("div", {"id": "wodinnerind"})

    if soup:
        for row in soup:
            temp_text = row.findAll(text=True)
            post_text = unidecode.unidecode('\n'.join(temp_text))
        title = (content.find("div", {"id": "wodbottomind"}).text).replace("Posted on ", "")

        text = "".join(str(tbl.prettify('latin-1')) for tbl in soup) if soup else None
        if re.search(IMG_REGEX, text) is not None:
            image = (base + str(re.search(IMG_REGEX, text).group(1))).replace("t-", "")


        post = Post(current_id, title, image, url, str(post_text).replace("'", "").replace("\"",""))
        return post
    return "0"