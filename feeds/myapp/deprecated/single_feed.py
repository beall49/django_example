import sys
if sys.version_info[0] == 2:
    from urllib import urlopen
    import send_to_parse as parse
else:
    from urllib.request import urlopen
    import feeds.myapp.api.send_to_parse as parse

from bs4 import BeautifulSoup
from collections import namedtuple

import sqlite3 as sqlite
import datetime
import re


class Feed:
    def __init__(self):
        self.DB_NAME = '/home/rbeall49/feeds/feeds/myapp/wod.db'
        self.base = "http://www.crossfitexcel.com"
        self.page = self.base + "/wod/{0}"
        self.current_id = 605
        self.NO_WOD_TEXT = "We are sorry, but this Wod does not exist!"

        self.image = 'http://www.crossfitexcel.com/images/logo.png'
        self.IMG_REGEX = r"<img[^>]+src\s*=\s*['\"]([^'\"]+)['\"][^>]*>"
        self.CREATE_TABLE = 'DROP TABLE IF EXISTS tbl_articles; CREATE TABLE tbl_articles (id INTEGER PRIMARY KEY AUTOINCREMENT, wodid INTEGER, title TEXT, img TEXT,  url TEXT, txt TEXT, DATEADD DATETIME DEFAULT CURRENT_TIMESTAMP)'
        self.INSERT = "INSERT INTO tbl_articles (current_id, title, image, url, post_text) VALUES ({0},'{1}','{2}','{3}','{4}')"
        self.MAX_ID = 'select max(current_id) FROM tbl_articles where current_id'

        self.Post = namedtuple('Post', 'current_id, title, image, url, post_text')
        self.now = datetime.datetime.now()


    """
        get last wod posted
    """
    def get_current_id(self):
        with sqlite.connect(self.DB_NAME) as con:
            cur = con.cursor()
            record_set = cur.execute(self.MAX_ID)
            for row in record_set:
                self.current_id = int(row[0]) + 1 if row[0] else self.current_id

    """
        insert record
    """
    def insert_post(self, post):
        with sqlite.connect(self.DB_NAME) as con:
            cur = con.cursor()
            if cur.execute("select * from tbl_articles where current_id={0}".format(post.current_id)).fetchone() is None:
                INSERT = self.INSERT.format(post.current_id, post.title, post.image, post.url, post.post_text)
                cur.execute(INSERT)
                parse.save_new_record()
                return '{"New Record Inserted:"' + str(post.current_id) + '}'

    """
        going to use this to select data
    """
    def sql_select(self):
        with sqlite.connect('wod.db') as con:
            cur = con.cursor()
            cur.execute(self.CREATE_TABLE)
            return cur.fetchall()

    """
        check for new record
    """
    def single_post(self, current_id):
        import unidecode
        url = self.page.format(current_id)
        title = self.now.strftime("%A")  + " " + self.now.strftime("%B")  + " " + self.now.strftime("%d") + ", " + self.now.strftime("%Y")
        post_text = self.NO_WOD_TEXT

        html = urlopen(url).read()
        content = BeautifulSoup(html, 'html.parser')
        soup = content.findAll("div", {"id": "wodinnerind"})


        if soup:
            for row in soup:
                temp_text = row.findAll(text=True)
                #.encode('ascii', 'replace')
                post_text = unidecode.unidecode('\n'.join(temp_text))
            title = (content.find("div", {"id": "wodbottomind"}).text).replace("Posted on ", "")

            text = "".join(str(tbl.prettify('latin-1')) for tbl in soup) if soup else None
            if re.search(self.IMG_REGEX, text) is not None:
                self.image = (self.base + str(re.search(self.IMG_REGEX, text).group(1))).replace("t-", "")


            post = self.Post(current_id, title, self.image, url, str(post_text).replace("'", "").replace("\"",""))
            return self.insert_post(post)

        return '{"No New Record Inserted:"' + str(current_id) + '}'

