from django.conf.urls import patterns, include, url
from django.contrib import admin
admin.autodiscover()
from feeds.myapp import data

urlpatterns = patterns('',

    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', 'feeds.myapp.views.input'),

    ### API MAP ###
    url(r'^api/update', 'feeds.myapp.api.api_map.check_for_update'),
    url(r'^api/wod/(?P<current_id>.*)', 'feeds.myapp.api.api_map.return_single_day'),
    url(r'^api/date/(?P<incoming_date>.*)', 'feeds.myapp.api.api_map.get_record_by_date'),


    url(r'^api/entries/(?P<records>.*)', 'feeds.myapp.api.api_map.get_records'),
    url(r'^api', 'feeds.myapp.api.api_map.get_current_id'),


    ### CK ###
    url(r'^ck/items', 'feeds.myapp.api.api_map.ck_data'),


    url(r'^feed/', 'feeds.myapp.views.returnfeed'), #, name='nothome'
    url(r'^wordList/(?P<strParm>.*)', 'feeds.myapp.views.wordList', name='wordList'),
    url(r'^syllables/(?P<strParm>.*)', 'feeds.myapp.views.syllables', name='syllables'),
    url(r'^data/', data.Wods.as_view(), name='data')
)


    # Examples:
    # url(r'^blog/', include('blog.urls')),
    # url(r'^', include(router.urls)),
    # url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    # url(r'^$',      'feeds.myapp.views.home', name='home'),
    #url(r'^input/' , 'feeds.myapp.views.input'),